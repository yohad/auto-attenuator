﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Auto_Attenuator
{
    class PingSender
    {
        public PingSender() { }

        Ping pingsender = new Ping();

        public bool OnePing(string IPReciver)
        {
            try
            {
                PingReply reply = pingsender.Send(IPReciver);
                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
