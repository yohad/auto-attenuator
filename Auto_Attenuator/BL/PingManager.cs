﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Auto_Attenuator;

namespace Auto_Attenuator.BL
{
    class PingManager
    {
        PingSender Ping = new PingSender();

        public PingManager() { }

        public int Recivercheck(bool withping, string IPReciver)
        {
            int reciver = 0;
            if (withping == true)
            {
                if (IPReciver == "")
                {
                    if (MessageBox.Show("?לשלוח פינגים", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    {
                        reciver = -1;
                        return reciver;
                    }
                    else
                    {
                        reciver = -2;
                        return reciver;
                    }

                }
                else
                {
                    for (int NumPing = 0; NumPing < Constants.number_pings; NumPing++)
                    {
                        if (OnePing(IPReciver))
                        {
                            reciver++;
                        }
                    }
                    return reciver;
                }
            }
            else
            {
                reciver = 1;
                return reciver;
            }
        }

        public bool OnePing(string IPReciver)
        {
            return Ping.OnePing(IPReciver);
        }

        
    }
}
