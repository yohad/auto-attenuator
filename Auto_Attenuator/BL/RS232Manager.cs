﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Auto_Attenuator
{
    class RS232Manager
    {
        private string portName;
        Rs232Pex Rs232_Pex;

        public RS232Manager()
        {
            Rs232_Pex = new Rs232Pex("com1");
        }

        public void init(string portsname)
        {
            try
            {
                portName = portsname;
                Rs232_Pex = new Rs232Pex(portName);
            }
            catch { }
        }

        public void SendOne()
        {
            byte[] SendOne = new byte[] { 1 };
            Rs232_Pex.SendingCommand(SendOne);
        }

        public void SendZero()
        {
            byte[] SendZero = new byte[] { 0 };
            Rs232_Pex.SendingCommand(SendZero);
        }

        public void SendOneOne()
        {
            byte[] SendOneOne = new byte[] { 1, 1 };
            Rs232_Pex.SendingCommand(SendOneOne);
        }

        public double SendUserChanges( double plus_or_minus , double two_rise )
        {
            double sum;
            byte[] rising = new byte[] { (byte) plus_or_minus , (byte)two_rise };
            Rs232_Pex.SendingCommand(rising);
            sum = ReadRS232();
            return sum;
        }

        public void UserMinus( byte[] down)
        {
            Rs232_Pex.SendingCommand(down);
        }

        
        

        public double ReadRS232()
        {
            double now, sum;
            Rs232_Pex.ClosePort(); //to clean the buffer
            Rs232_Pex.OpenPort();
            now = Rs232_Pex.ReadNow();
            sum = now / 2;
            return sum;
        }

        public void OpenPort()
        {
            Rs232_Pex.OpenPort();
        }

        public void ClosePort()
        {
            Rs232_Pex.ClosePort();
        }

        public double HalfPlus()
        {
            double sum;
            SendOneOne();
            sum = ReadRS232();
            return sum;
        }


        public string[] GetportNames() { return Rs232_Pex.GetPortNames(); }
    }
}
