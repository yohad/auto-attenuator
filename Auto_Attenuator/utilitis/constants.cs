﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auto_Attenuator
{
    static class Constants
    {
        public const int number_pings=10;
        public const double max_attenuate = 31.5;
    }
}
