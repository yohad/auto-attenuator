﻿using Auto_Attenuator.BL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Auto_Attenuator;


namespace Auto_Attenuator
{
    // <summary>
    // Interaction logic for MainWindow.xaml
    // </summary>

    public partial class MainWindow : Window
    {
        CalculateManager CM = new CalculateManager();
        PingManager PM = new PingManager();
        RS232Manager RM = new RS232Manager();
        double sum = 0, checking_att;
        bool withping = true, autorise = true, exit = false, middle_autorising = false, on_click;


        public MainWindow()
        {
            InitializeComponent();
            PortNames();
        }


        


        private async void AutoRisingButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (Rise_After_Max())
                {
                    if (CM.Frequency(AutoRisingTxt.Text) != -1)
                    {
                        middle_autorising = true;
                        exit = false;
                        for (; sum < Constants.max_attenuate;)
                        {
                            if (exit == false)
                            {
                                if (ReciverCheck())
                                {
                                    sum = RM.HalfPlus(); 
                                    checking_att = CM.checking("plus", checking_att, 0.5);
                                    if (checking_att == sum)
                                    {
                                        AttenuateNow.Text = sum.ToString();
                                        MaxAtt(sum);
                                    }
                                    else
                                    {
                                        MsgBox.Problem();
                                    }
                                    await Task.Delay(CM.Frequency(AutoRisingTxt.Text));
                                }
                            }
                            else
                                break;
                        }

                    }
                    Off_Click();
                    middle_autorising = false;
                }
                
            }

            
        }

        private void UserPlusButton_Click(object sender, RoutedEventArgs e)
        {

            if (!On_Click())
            {
                if (Rise_After_Max())
                {
                    if (UserControlTxt.Text != "")
                    {
                            if (CM.Two_User(UserControlTxt.Text) == 0) 
                                UserChange_NotOK();
                            else
                            {

                                if (Rise_Or_Down_Limits(sum,1, UserControlTxt.Text))
                                {
                                    
                                    if (ReciverCheck())
                                    { 
                                        sum = RM.SendUserChanges(1, CM.Two_User(UserControlTxt.Text));
                                        checking_att = CM.checking("plus", checking_att, double.Parse(UserControlTxt.Text));

                                        if (checking_att == sum)
                                        {
                                            AttenuateNow.Text = sum.ToString();
                                        }
                                        else
                                        {
                                            MsgBox.Problem();
                                        }
                                        MaxAtt(sum);

                                    }
                                }
                                else
                                    MsgBox.Changing_Limit();
                            }
                       
                    }
                    else
                        UserChange_NotOK();
                }
                Off_Click();
            }
        }

        private void HalfPlusButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (Rise_After_Max())
                {
                   
                        if (ReciverCheck())
                        {
                            sum = RM.HalfPlus();
                            checking_att = CM.checking("plus", checking_att, 0.5);
                            if (checking_att == sum)
                            {
                                AttenuateNow.Text = sum.ToString();
                            }
                            else
                            {
                                MsgBox.Problem();
                            }
                            MaxAtt(sum);

                        }
                }
                Off_Click();
            }
        }

        private async void UserMinusButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (!On_Click())
            { 
                    if (sum > 0)
                    {
                        if (UserControlTxt.Text != "")
                        {

                            if (CM.Two_User(UserControlTxt.Text) == 0)
                                UserChange_NotOK();
                            else
                            {

                                if (Rise_Or_Down_Limits(sum, 0, UserControlTxt.Text))
                                {

                                    byte[] downing = new byte[] { (byte)CM.Two_User(UserControlTxt.Text) };

                                    RM.SendZero();
                                    await Task.Delay(50);
                                    RM.SendOne();
                                    await Task.Delay(50);
                                    RM.UserMinus(downing);

                                    sum = RM.ReadRS232();
                                    checking_att = CM.checking("minus", checking_att, double.Parse(UserControlTxt.Text));
                                    if (checking_att == sum)
                                    {
                                        AttenuateNow.Text = sum.ToString();
                                    }
                                    else
                                    {
                                    MsgBox.Problem();
                                    }
                                    MaxAtt(sum);


                                }
                                else
                                    MsgBox.Changing_Limit();
                            }
                        }
                   
                    }
                    Off_Click();
                }
        }

 

        private async void ZeroButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                    RM.SendZero();
                    await Task.Delay(50);
                    RM.SendZero();
                    await Task.Delay(50);
                    RM.SendZero();

                    sum = RM.ReadRS232();
                    checking_att = 0;
                    if (checking_att == sum)
                    {
                        AttenuateNow.Text = sum.ToString();
                    }
                    else
                    {
                        MsgBox.Problem();
                    }

                    MaxAtt(sum);


                Off_Click();
            }
        }

        private async void HalfMinusButton_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                if (sum > 0)
                {
                    RM.SendZero();
                    await Task.Delay(50);
                    RM.SendOne();
                    await Task.Delay(50);
                    RM.SendOne();
                    sum = RM.ReadRS232();
                    checking_att = CM.checking("minus", checking_att, 0.5);
                    if (checking_att == sum)
                    {
                        AttenuateNow.Text = sum.ToString();
                    }
                    else
                    {
                        MsgBox.Problem();
                    }
                    MaxAtt(sum);

                }
                Off_Click();
            }
        }






        public bool ReciverCheck()
        {           
            int reciver = 0;

            reciver = PM.Recivercheck(withping, IpReciverText.Text);
            if (reciver == -1)
            {
                WithOrWithoutButton.IsChecked = true;
                WithOrWithoutButton_Click(null, null);
                return true;
            }
            else if (reciver == -2)
            {
                IpReciverText.Focus();
                return false;
            }
            else if (reciver == 0)
            {
                MsgBox.PowerTranssmit(sum);
                return false;
            }
            else
                return true;   
            
        }

        private void AutoexitButton_Click(object sender, RoutedEventArgs e)
        {
            exit = true;
            middle_autorising = false;
            Off_Click();
        }
   
        private void sendingPing_Click(object sender, RoutedEventArgs e)
        {
            if (!On_Click())
            {
                bool ping_reply;


                ping_reply = PM.OnePing(IpReciverText.Text);
                if (ping_reply)
                {
                    IPTrue.Visibility = Visibility.Visible;
                    IPFalse.Visibility = Visibility.Hidden;
                }
                else
                {
                    IPFalse.Visibility = Visibility.Visible;
                    IPTrue.Visibility = Visibility.Hidden;
                }
                Off_Click();
            }
           
            
        }

        public void WithOrWithoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (withping == true)
            {
                withping = false;
                sendingPing.IsEnabled = false;
                IpReciverText.IsEnabled = false;
                IPTrue.Visibility = Visibility.Hidden;
                IPFalse.Visibility = Visibility.Hidden;
               
                Thread.Sleep(100);
                

            }
            else
            {
                withping = true;
                sendingPing.IsEnabled = true;
                IpReciverText.IsEnabled = true;

            }
        }

        public void Auto_Manual(bool autoenable, bool manual)
        {

            AutoRisingButton.IsEnabled = autoenable;
            AutoRisingTxt.IsEnabled = autoenable;
            AutoexitButton.IsEnabled = autoenable;
            HalfPlusButton.IsEnabled = manual;
            HalfMinusButton.IsEnabled = manual;
            UserControlTxt.IsEnabled = manual;
            UserMinusButton.IsEnabled = manual;
            UserPlusButton.IsEnabled = manual;
        }

        private void CheckNumInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void EnableAutoRising_Click(object sender, RoutedEventArgs e)
        {
            if (autorise == true)
            {
                autorise = false;
                Auto_Manual(false, true);
                exit = true;
             
            }
            else
            {
                autorise = true;
                Auto_Manual(true, false);
            }
        }

        public void MaxAtt(double max)
        {
            if (max == Constants.max_attenuate)
            {
                MaxAttenuate.Visibility = Visibility.Visible;
                HalfPlusButton.IsEnabled = false;
                UserPlusButton.IsEnabled = false;
                exit = true;
                
            }
            else
            {
                MaxAttenuate.Visibility = Visibility.Hidden;
                if (middle_autorising == false)
                {
                    HalfPlusButton.IsEnabled = true;
                    UserPlusButton.IsEnabled = true;
                }
            }
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            double first=-1;
            
          
        
                first = CheckingConnection();
                if (first >= 0)
                {
                    ConnectButton.IsEnabled = false;
                    DisconnectIcon.Visibility = Visibility.Hidden;
                    ConnectIcon.Visibility = Visibility.Visible;

                    PortNameComboBox.IsEnabled = false;
                    sum = RM.ReadRS232();
                    checking_att = sum;
                    AttenuateNow.Text = sum.ToString();
                    On_Connection(true);

                }
                else
                {
                    DisconnectIcon.Visibility = Visibility.Visible;
                    return;
                }
         

        }

        public async void UserChange_NotOK()
        {
            UserCtrlFalse.Visibility = Visibility.Visible;
            UserControlTxt.Text = "";
            UserControlTxt.Focus();
            await Task.Delay(1000);
            UserCtrlFalse.Visibility = Visibility.Hidden;
        }

        private void PortNameComboBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            RM.init(PortNameComboBox.Text);
            RM.OpenPort();
        }

        public bool On_Click()
        {
            if (on_click)
                return true;
            else
            {
                on_click = true;
                return false;
            }
        }

        public void Off_Click()
        {
            on_click = false;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double height, ratio;

            double newWindowHeight = e.NewSize.Height;
            double newWindowWidth = e.NewSize.Width;
            double prevWindowHeight = e.PreviousSize.Height;
            double prevWindowWidth = e.PreviousSize.Width;

            height = newWindowHeight - prevWindowHeight;
            ratio = height / 24;
            ratio -= ratio % 1;

        }

        public bool Rise_After_Max()
        {
            if (sum < Constants.max_attenuate)
                return true;
            else
                return false;
        }

        public bool Rise_Or_Down_Limits(double sum, int PlusOrMinus, string UserChange)
        {
            double UserChanges;
            UserChanges = double.Parse(UserChange);
            if (PlusOrMinus == 1)
            {
                if (sum + UserChanges < Constants.max_attenuate) return true;
                else return false;

            }
            else
            {
                if (sum - UserChanges > 0) return true;
                else return false;
            }
        }

        public void PortNames()
        {
            string[] portsName = RM.GetportNames();
            foreach ( string portname in portsName)
            {
                PortNameComboBox.Items.Add(portname);
            }
        }

        public double CheckingConnection()
        {
            double first = -1;
            int timer = 0;
           
              
                Task read = Task.Factory.StartNew(() =>
                {
                    first = RM.ReadRS232();

                });
                Task<double> time = Task<double>.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        timer++;
                        Thread.Sleep(1000);
                        if (first >= 0)
                            return first;
                        if (timer == 1)
                        {
                            first = -2;
                            return first;
                        }

                    }
                });
                if (time.Result == -2)
                    return -2;
                else
                    return first;
               
               
            

        }

        public void On_Connection(bool enable)
        {
            WithOrWithoutButton.IsEnabled = enable;
            IpReciverText.IsEnabled = enable;
            sendingPing.IsEnabled = enable;
            EnableAutoRising.IsEnabled = enable;
            AutoRisingTxt.IsEnabled = enable;
            AutoRisingButton.IsEnabled = enable;
            AutoexitButton.IsEnabled = enable;
            ZeroButton.IsEnabled = enable;
        }


    }
}



