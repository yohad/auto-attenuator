﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Auto_Attenuator
{
    public static class MsgBox
    { 

        public static void ZeroAttenuate()
        {
            MessageBox.Show("הניחות עומד על 0 לא ניתן להוריד יותר");
        }

        public static void PowerTranssmit(double sum)
        {
            MessageBox.Show(string.Format("עוצמת השידור היא {0} dbm", sum));
        }

        public static void EmptyFreq()
        {
            MessageBox.Show("יש לבחור תדירות בתחום");
        }

        public static void Problem()
        {
            MessageBox.Show("יש בעיה עם הקריאה או עם הכתיבה למעגל הערך שהוא מחזיר אינו הערך הנדרש, אם כיבית את המעגל הפעל מחדש את התוכנה");
        }

        public static void Changing_Limit()
        {
            MessageBox.Show("המספר שאתה מבקש אינו בתחום");
        }

    }
}
